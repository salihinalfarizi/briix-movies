import FormPage from 'src/pages/FormPage.vue'
import SplashScreen from 'src/components/SplashScreen.vue'
import MovieDetailPage from 'src/pages/MovieDetailPage.vue'
import TheMovieDbFetch from 'src/pages/PostFetchDataExample.vue'
const routes = [
  {
    path: '/',
    name: 'Splash',
    component: SplashScreen
  },
  {
    path: '/home',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/IndexPage.vue') },
      {
        path: '/form/:mode/:index?', name: 'form', component: FormPage
      },
      {
        path: '/movie/:index',
        name: 'movieDetail',
        component: MovieDetailPage
      }
    ]
  },
  {
    path: '/fetch-themoviedb',
    name: 'TheMovieDbFetch',
    component: TheMovieDbFetch
  },
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes


