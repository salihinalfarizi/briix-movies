export default {
    allMovies: state => state.movies,
    isLoading: state => state.loading,
    fetchError: state => state.error
};