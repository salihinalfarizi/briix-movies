import axios from 'axios';

export default {
  async fetchNowPlayingMovies({ commit }, pageNumber) {
    commit('SET_LOADING', true);
    commit('SET_ERROR', null);

    const options = {
      method: 'GET',
      headers: {
        accept: 'application/json',
        Authorization: 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIyMTE1ODU4NTVlN2MwYzBhOWU1ZjdkNWMyY2QwN2Y1MyIsInN1YiI6IjY1Zjc0YTgwODFkYTM5MDE4NjYzNzA5NyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.beaQvKRkFEvLcegYH6ASYDI4fFyRpFW4ILC8au6W3Ek'
      }
    };

    try {
      const response = await axios.get(`https://api.themoviedb.org/3/movie/now_playing?language=en-US&page=${pageNumber}`, options);
      commit('APPEND_MOVIES', response.data.results);
    } catch (error) {
      commit('SET_ERROR', error);
    } finally {
      commit('SET_LOADING', false);
    }
  }
};
