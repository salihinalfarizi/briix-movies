export default {
    SET_MOVIES(state, movies) {
      state.movies = movies;
    },
    APPEND_MOVIES(state, movies) {
      state.movies = [...state.movies, ...movies];
    },
    SET_LOADING(state, isLoading) {
      state.loading = isLoading;
    },
    SET_ERROR(state, error) {
      state.error = error;
    }
  };
  